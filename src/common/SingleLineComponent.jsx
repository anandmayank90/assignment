import { Box, Link, Typography } from "@mui/material";

export const SingleLineComponent = ({
  label,
  value,
  labelProps,
  valueProps,
  id,
  link,
  linkProps,
  boxSx
}) => {
  return (
    <Box
      display={"flex"}
      key={`${label}_${id}`}
      sx={{ flexDirection: 'row',...boxSx }}
    >
      {label && (
        <Typography {...labelProps } noWrap>
          {label}
        </Typography>
      )}
      {value && (
        <Typography {...valueProps} noWrap>
          {value}
        </Typography>
      )}
      {link && (
        <Link href={`mailto:${link}`} {...linkProps} noWrap onClick={(e)=>{
            e.stopPropagation();
        }}>
          {link}
        </Link>
      )}
    </Box>
  );
};
SingleLineComponent.defaultProps={
    boxSx:{}
}