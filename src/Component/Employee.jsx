import { SingleLineComponent } from "@/common/SingleLineComponent";
import { Card, CardContent, Grid, Stack } from "@mui/material";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";

export const Employee = () => {
  const router = useRouter();
  const employeeList = useSelector((state) => state?.list?.list);
  return (
    <Grid
            container
            justifyContent={"center"}
            alignItems={"center"}
            marginTop={4}
          >
      {employeeList.map((employee) => {
        return (
          <Grid item xs={12} sm={7} marginTop={5}>
            <Card variant="outlined" sx={{ cursor: "pointer" }}>
              <CardContent
                onClick={() => {
                  router.push(`/list/${employee.id}`);
                }}
              >
                <Stack gap={2}>
                  <SingleLineComponent
                    label="Employee First Name:"
                    value={employee?.first_name || ""}
                    valueProps={{
                      variant: "h7",
                      paddingLeft: 1,
                      fontWeight: 500,
                    }}
                    id={employee?.id || ""}
                    labelProps={{ variant: "h7", fontWeight: 700 }}
                  />
                  <SingleLineComponent
                    label="Employee Last Name:"
                    value={employee?.last_name || ""}
                    valueProps={{
                      variant: "h7",
                      fontWeight: 500,
                      paddingLeft: 1,
                    }}
                    id={employee?.id || ""}
                    labelProps={{ variant: "h7", fontWeight: 700 }}
                  />
                  <SingleLineComponent
                    label="Email:"
                    link={employee?.email || ""}
                    linkProps={{
                      variant: "h7",
                      fontWeight: 500,
                      paddingLeft: 1,
                      sx: { color: "blue", zIndex: 2 },
                    }}
                    id={employee?.id || ""}
                    labelProps={{ variant: "h7", fontWeight: 700 }}
                  />
                </Stack>
              </CardContent>
            </Card>
          </Grid>
        );
      })}
      )
    </Grid>
  );
};
