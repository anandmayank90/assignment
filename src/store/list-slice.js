import { fetchEmployee, fetchList } from "./thunk/list-thunk";

const { createSlice } = require("@reduxjs/toolkit");
let initialState = {
  selectedList: null,
  list: [],
  totalPages: 0,
  page: 0,
  loadingState: {
    homePageLoading: true,
    selectedEmployeeLoading: true,
  },
};
const listSlice = createSlice({
  name: "list",
  initialState: initialState,
  reducers: {
    setList(state, { payload }) {
        let selectedEmployee = state.list.find(
            (employee) => `${employee.id}` === payload
          );
      return {
        ...state,
        selectedList: selectedEmployee,
      };
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchList.pending, (state) => {
        state.loadingState.homePageLoading = true;
      })
      .addCase(fetchList.fulfilled, (state, { payload }) => {
        const { total_pages, data, page } = payload;
        state.loadingState.homePageLoading = false;
        state.totalPages = total_pages || 0;
        state.page = page || 1;
        state.list = data;
      })
      .addCase(fetchList.rejected, (state) => {
        state.loadingState.homePageLoading = false;
      })
      .addCase(fetchEmployee.pending, (state) => {
        state.loadingState.selectedEmployeeLoading = true;
      })
      .addCase(fetchEmployee.fulfilled, (state, { payload }) => {
        state.loadingState.selectedEmployeeLoading = false;
        state.selectedList = payload;
      })
      .addCase(fetchEmployee.rejected, (state) => {
        state.loadingState.selectedEmployeeLoading = false;
      });
  },
});
export const listAction = listSlice.actions;
export default listSlice;
