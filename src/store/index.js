import { configureStore } from "@reduxjs/toolkit";
import listSlice from "./list-slice";

export function makeStore(){
    return configureStore({
        reducer:{
            list:listSlice.reducer
        }
    })
}
const store=makeStore();
export default store;