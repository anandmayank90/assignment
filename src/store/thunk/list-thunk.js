import { getList } from "@/service/list";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const fetchList=createAsyncThunk('list/fetchList',async(payload=1,{rejectWithValue})=>{
try {
    const {data,error}= await getList(payload);
console.log(data);
    if(data){
        return data;
    }
    else if(error){
        rejectWithValue()
    }
}catch(error){
   return rejectWithValue()
}

})
export const fetchEmployee=createAsyncThunk('list/fetchEmployee',async(payload,{rejectWithValue})=>{
    try {
        const {data,error}= await getList(payload.page);
   
        if(data){
            const {data:employees}=data;
            let selectedEmployee = employees.find(
                (employee) => `${employee.id}` === payload.employeeId
              );
            return selectedEmployee;
        }
        else if(error){
            rejectWithValue()
        }
    }catch(error){
       return rejectWithValue()
    }
    
    })