import { SingleLineComponent } from "@/common/SingleLineComponent";
import { listAction } from "@/store/list-slice";
import { fetchEmployee, fetchList } from "@/store/thunk/list-thunk";
import { Avatar, Box, CircularProgress, Container, Stack, Typography,useMediaQuery } from "@mui/material";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

const List = () => {
  const router = useRouter();
  const isMobile=useMediaQuery('(max-width:600px)')
  const { id } = router.query;
  const page = useSelector((state) => state?.list?.page);
  const list = useSelector((state) => state?.list?.selectedList);
  const employees = useSelector((state) => state?.list?.list);
  const selectedEmployeeLoading = useSelector(
    (state) => state?.list?.loadingState.selectedEmployeeLoading
  );
  const dispatch = useDispatch();
  useEffect(() => {
    if (id / 5 + 1 === page) {
      let selectedEmployee = employees.find(
        (employee) => `${employee.id}` === id
      );
      dispatch(listAction.setList(id));
    } else {
      dispatch(fetchList(id / 5 + 1));
      dispatch(fetchEmployee({ page: id / 5 + 1, employeeId: id }));
    }
  }, [id]);

  return (
    <>
      <Container sx={{ my: 5 }} overflow={"hidden"}>
        {selectedEmployeeLoading && (
          <Box
            display={"flex"}
            justifyContent={"center"}
            alignItems={"center"}
            height="80vh"
          >
            <CircularProgress />
          </Box>
        )}
        {list &&
          !selectedEmployeeLoading&&(
            <Box>
              <Stack alignItems={"center"}>
                <Avatar
                  alt="Remy Sharp"
                  src={list.avatar}
                  sx={{ width: 300, height: 300, mb: 5 }}
                />
              </Stack>
              <Stack alignItems={"center"}>
                <SingleLineComponent
                  label="Employee First Name:"
                  value={list?.first_name || ""}
                  valueProps={{
                    variant: isMobile?'h7':'h6',
                    paddingLeft: 1,
                    fontWeight: 500,
                  }}
                  id={list?.id || ""}
                  labelProps={{ variant: isMobile?'h7':'h6', fontWeight: 700 }}
                  boxSx={{flexDirection:'row',mb:2}}
                />
                <SingleLineComponent
                  label="Employee Last Name:"
                  value={list?.last_name || ""}
                  valueProps={{
                    variant: isMobile?'h7':'h6',
                    paddingLeft: 1,
                    fontWeight: 500,
                  }}
                  id={list?.id || ""}
                  labelProps={{ variant: isMobile?'h7':'h6', fontWeight: 700 }}
                  boxSx={{flexDirection:'row',mb:2}}
                />
                <SingleLineComponent
                  label="Employee Email:"
                  link={list?.email || ""}
                  linkProps={{
                    variant: isMobile?'h7':'h6',
                    paddingLeft: 1,
                    fontWeight: 500,
                  }}
                  id={list?.id || ""}
                  labelProps={{ variant: isMobile?'h7':'h6', fontWeight: 700 }}
                  boxSx={{flexDirection:'row',mb:2}}
                />
              </Stack>
            </Box>
          )}
        {!list &&
          !selectedEmployeeLoading&&(
            <Stack
              justifyContent={"center"}
              alignItems={"center"}
              height="80vh"
            >
              <Typography variant="h2">404 No Employee Found</Typography>
            </Stack>
          )}
      </Container>
    </>
  );
};

export default List;
