import { Employee } from "@/Component/Employee";
import { fetchList } from "@/store/thunk/list-thunk";
import {
  Box,
  CircularProgress,
  Container,
  Pagination,
  Typography
} from "@mui/material";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

export default function Home() {
  const router = useRouter();
  const dispatch = useDispatch();
  const isLoading = useSelector(
    (state) => state?.list?.loadingState.homePageLoading
  );
  const employeeList = useSelector((state) => state?.list?.list);
  const totalPages = useSelector((state) => state?.list?.totalPages);
  const currentPage = useSelector((state) => state?.list?.page);
  useEffect(() => {
    // used timeout to show loading component for heavy api's
    setTimeout(() => {
      dispatch(fetchList());
    }, 2000);
  }, []);
  return (
    <main>
      {isLoading ? (
        <Box
          display={"flex"}
          justifyContent={"center"}
          alignItems={"center"}
          height="100vh"
        >
          <CircularProgress />
        </Box>
      ) : (
        <Container sx={{ my: 5 }}>
          <Typography variant="h4" textAlign={"center"}>
            Welcome to Employee Portal
          </Typography>

          {employeeList && <Employee />}
        </Container>
      )}
      {currentPage !== 0 && (
        <Box display={"flex"} justifyContent={"center"} alignItems={"center"}>
          <Pagination
            size={"large"}
            count={totalPages}
            page={currentPage}
            onChange={(event, value) => {
              dispatch(fetchList(value));
            }}
            color="primary"
            sx={{
              mb:5,
              "& .MuiPaginationItem-root": {
                fontSize: "16px",
              },
            }}
          />
        </Box>
      )}
    </main>
  );
}
