import store from "../store"
import { Box } from "@mui/material"
import { Provider } from "react-redux"

const MyApp=({Component})=>{
    return (
    <Provider store={store}>
    <Box>
       
        <Component />
    </Box>
    </Provider>
    )

}
export default MyApp