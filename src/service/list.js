import axios from 'axios';
export const getList=async(page=1)=>{
return await axios.get(`https://reqres.in/api/users?page=${page}&per_page=5`).then((res)=>{
    
   return {data:res.data};
}).catch((error)=>{
    return error;
})
}